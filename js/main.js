let nav = document.getElementById("nav__links");

const navController = () => {

    if (nav.className === "nav__links") {
        nav.className += " nav__mobile";
    } else {
        nav.className = "nav__links";
    }
}

document.querySelectorAll('.nav__links a').forEach(link => {
    link.addEventListener('click', () => {
        nav.className = "nav__links";
    })
})